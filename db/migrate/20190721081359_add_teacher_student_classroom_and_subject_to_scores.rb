class AddTeacherStudentClassroomAndSubjectToScores < ActiveRecord::Migration[5.2]
  def change
    add_reference :scores, :student, foreign_key: true
    add_reference :scores, :classroom, foreign_key: true
    add_reference :scores, :subject, foreign_key: true
  end
end
