# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#

subjects = [
  "IPS",
  "IPA",
  "Sejarah",
  "Matematika",
  "Bahasa Inggris",
  "Bahasa Indonesia"
].each do |s|
  Subject.create(name: s)
end
puts "Subjects created."

8.times do
  Teacher.create(name: Faker::Name.name)
end
puts "Teachers created"

classroom = [
  "2A",
  "2B",
  "2C",
  "2D",
  "3A",
  "3B",
  "3C",
  "3D"
].each_with_index do |c, i|
  Classroom.create(name: c, homeroom_teacher: Teacher.find(i + 1))
end

puts "Classrooom created."

20.times do
  Schedule.create(
    teacher: Teacher.find(Faker::Number.between(1,8)),
    subject: Subject.find(Faker::Number.between(1,5)),
    classroom: Classroom.find(Faker::Number.between(1, 8))
  )
end
puts "Schedul Created."

50.times do
  student = Student.create(
    name: Faker::Name.name,
    classroom: Classroom.find(Faker::Number.between(1, 8))
  )
  Subject.all.each do |subject|
    student.scores.create(
      classroom: student.classroom,
      subject: subject,
      value: Faker::Number.between(10, 100),
      semester: 1
    )

    student.scores.create(
      classroom: student.classroom,
      subject: subject,
      value: Faker::Number.between(10, 100),
      semester: 2
    )
  end

  puts "Student created"
end
