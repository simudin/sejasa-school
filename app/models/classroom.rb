class Classroom < ApplicationRecord
  has_many :students
  belongs_to :homeroom_teacher, class_name: "Teacher", foreign_key: "teacher_id"

  def self.all_students_of(classroom_name)
    Student.joins(:classroom)
      .where("classrooms.name = ?", classroom_name)
  end

  def self.rank_by(classroom_name, semester)
    Student.joins(:classroom, :scores)
      .where("classrooms.name = ? AND scores.semester = ?", classroom_name, semester)
      .select("students.id, students.name, avg(scores.value) as average_scores")
      .group("students.id")
      .order("average_scores DESC")
  end
end
