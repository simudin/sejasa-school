class Teacher < ApplicationRecord
  has_one :classroom
  has_many :schedules
  has_many :classrooms, through: :schedules
  has_many :subjects, through: :schedules
end
