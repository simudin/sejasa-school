class Score < ApplicationRecord
  belongs_to :student
  belongs_to :classroom
  belongs_to :subject

  def self.biggest_of(subject_name)
    score = Score.joins(:subject)
      .where("subjects.name  = ?", subject_name)
      .order("scores.value desc")
      .first

    return { name: score.student.name, score: score.value }
  end
end
