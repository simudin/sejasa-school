# Sejasa School

Model system of Sejasa School Information System.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

What things you need to install:

```
ruby 2.6.3
rails 5.2
graphviz

```

### Installing

```
bundle install
```

Run the rake commands for database and data seed

```
rake db:create
rake db:migrate
rake db:seed
```

Since this app only have models, you can only interact with the rails console

```
rails c
```

### All available methods 

To find all student from specific class
```
Classroom.all_students_of(classroom_name)
// classroom_name is a string

example:
Classroom.all_students_of("2A")
```

To find students rank based on specific class and based on the semester
```
Classroom.rank_by(classroom_name, semester)
// classroom_name is a string
// semester is an integer

example:
Classroom.rank_by("3A", 1)
```

To the student with the biggest score based on specific subject
```
Score.biggest_of(subject_name)
// subject_name is a string

example:
Score.biggest_of("IPA")
```
